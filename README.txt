This module creates a simple graphical calendar block using jQuery and
jQueryUI(datepicker).Datepicker plugin does not support Persian calendar, but it's
been edited nicely by 'hasheminezhad' so it does. This module is just a graphical
block, if you need to add new calendar systems to drupal(like Iranian Shamsi,...)
check out 'Calendar Systems' module, which most of the job is already done there.

Dependencies
------------
- jquery_ui
If you had problems with your jQuery version and integrating with block, you may
 get better performance installing 'jquery_update' module too.

Install
-------
- Copy 'jquery.ui.datepicker-cc.js', 'jquery.ui.datepicker-cc-ar.js', 'jquery.ui.datepicker-cc-fa.js' from '/graphical_calendar/calendar' in modules folder to ‘libraries/jquery.ui/ui' folder(you’ve already had this folder if you have jquery_ui installed and there's also no need to replace anything).
- Enable module.
- You can now use the new block called 'Graphical Calendar' or call the API function.

API
-—-
For developers who want to use datepicker in modules or themes they can simply call the function datepicker_add()

datepicker_add($selector = '', $options = ‘’);

Adds datepicker plugin to current page or to CSS selector.
Example:
  datepicker_add('#from-date, #to-date', 'dateFormat: "yy-mm-dd",
    changeMonth: true, changeYear: true, yearRange: "1300:y"');
@param  string $selector (optional)
  Call the datepicker for a CSS selector. Use a full selector path
  like: 'body div.wrapper input#date'.
  If nothing set it adds library to current page but won't call the
  datepicker for any CSS path so you can add it yourself in theme or
  module.
@param  string $options  (optional)
  Some examples to use options:
  "showWeek: true"
  "dateFormat: 'dd/mm/yy', altField: '#alternate2', altFormat: 'DD، d MM yy'"
  "showButtonPanel: true"
  "dateFormat: 'dd/mm/yy', autoSize: true"
  "changeMonth: true, changeYear: true"
  To use Arabic calendar use: "regional: 'ar'" option(Jalali is default).
  To see more options read http://hasheminezhad.com/datepicker
